/*global Canvasing, Backbone*/

Canvasing.Routers = Canvasing.Routers || {};

(function () {
	'use strict';

	Canvasing.Routers.Global = Backbone.Router.extend({
		routes : {
			'' : 'login',
			'dashboard' : 'dashboard',
			'touch_point/:id' : 'touch_point',
		},
		login : function(){
			console.log('on login route');
			Canvasing.Views.login = new Canvasing.Views.Login();
			$('#login-screen-container').html(Canvasing.Views.login.render().el);
		},
		dashboard : function(){
			console.log('on dashboard route');
			// Canvasing.Views.dashboard = new Canvasing.Views.Dashboard();
			// $('#main-view').html(Canvasing.Views.dashboard.render().el);
		},
		touch_point : function(id){
			console.log('on touch point route');
			// Canvasing.Views.distribution = new Canvasing.Views.Distribution();
			// $('#sub-view').html(Canvasing.Views.distribution.render().el);
		},
	});

	Canvasing.Routers.router = new Canvasing.Routers.Global();

	Backbone.history.start();

})();
