var app = {
	initialize: function() {
		this.bindEvents();
	},
	bindEvents: function() {
		document.addEventListener('deviceready', this.onDeviceReady, false);
	},
	onDeviceReady: function() {
		app.receivedEvent('deviceready');
	},
	receivedEvent: function(id) {
		var parentElement = document.getElementById(id);
		var listeningElement = parentElement.querySelector('.listening');
		var receivedElement = parentElement.querySelector('.received');

		listeningElement.setAttribute('style', 'display:none;');
		receivedElement.setAttribute('style', 'display:block;');

	}
};



// main page render

function show_dashboard(){
	
	$('.canvas-page').addClass('hide');
	$('#dashboard').removeClass('hide');

	// change scren title
	$('#navbar-brand #screen-title').text('Dashboard : WE - Territory 1213');
	$('#navbar-brand #previous-screen').addClass('hide');
	setTimeout(function(){
		window.scrollTo(0,0)
	}, 5000);
	
	start_dashboard_chart();
}

function show_management(){
	
	$('.canvas-page').addClass('hide');
	$('#management').removeClass('hide');

	// change scren title
	$('#navbar-brand #screen-title').text('Management');
	$('#navbar-brand #previous-screen').addClass('hide');
	setTimeout(function(){
		window.scrollTo(0,0)
	}, 100);
}

function show_touchPoints(){
	
	$('.canvas-page').addClass('hide');
	$('#touch-points').removeClass('hide');

	// change scren title
	$('#navbar-brand #screen-title').text('Touch Points : WE - Territory 1213');
	$('#navbar-brand #previous-screen').addClass('hide');
	setTimeout(function(){
		window.scrollTo(0,0)
	}, 100);
	
	start_filter_map();
}

function show_single_touchPoints(){
	
	$('.canvas-page').addClass('hide');
	$('#touch-point-single').removeClass('hide');

	// change scren title
	$('#navbar-brand #screen-title').text('Toko Ajat');
	$('#navbar-brand #previous-screen').removeClass('hide');
	setTimeout(function(){
		window.scrollTo(0,0)
	}, 100);
	
	start_touch_point_chart();
}

function sign_in(){
	// hide login
	$('#login-screen').addClass('hide');

	// show alert
	$('#global-alert').removeClass('hide');
	$('#global-alert #message-content').html('Welcome !');
	setTimeout(function(){
		$('#global-alert').fadeOut('fast');
	}, 3000);

	//show navigation
	$('#top-bar').removeClass('hide');
	$('#main-navigation').removeClass('hide');
}

function sign_out(){
	// hide login
	$('.canvas-page').addClass('hide');
	$('#login-screen').removeClass('hide');

	// show alert
	$('#global-alert').removeClass('hide');
	$('#global-alert #message-content').html('You Are Logged Out !');

	//show navigation
	$('#top-bar').addClass('hide');
	$('#main-navigation').addClass('hide');
}
// component render

function start_dashboard_chart() {
	if (window.chart_dashboard_rendered == 0) {

		// Get the context of the canvas element we want to select
		var ctx = document.getElementById("donut-0").getContext("2d");
		var myNewChart = new Chart(ctx).Doughnut(top5brand);
		var ctx = document.getElementById("donut-1").getContext("2d");
		var myNewChart = new Chart(ctx).Doughnut(top5brand);
		var ctx = document.getElementById("bar-0").getContext("2d");
		var myBarChart = new Chart(ctx).Bar(salesBar);

		var ctx = document.getElementById("ksi-overview-bar").getContext("2d");
		var myBarChart = new Chart(ctx).Bar(ksiOverviewBar);

		window.chart_dashboard_rendered = 1;

		$('.dynamicsparkline').each(function(index, el) {
			day1 = random_1_100();
			day2 = random_1_100();
			day3 = random_1_100();
			day4 = random_1_100();
			day5 = random_1_100();

			var myvalues = [day1,day2,day3,day4,day5];
			if (day5 >= 50 ) {
				value = 'success';
			} else {
				value = 'danger';
			}
			$(this).sparkline(myvalues).addClass(value).after(' <span class="label label-'+value+'">'+day5+'</span>');
		});
	};
		//An example with all options.
		// var waTable = $('#ksi-global-table').WATable({}).data('WATable');  //This step reaches into the html data property to get the actual WATable object. Important if you want a reference to it as we want here.

		// var data = getData();
		// waTable.setData(data);  //Sets the data.
}

function start_filter_chart() {
	// if (window.chart_filter_rendered == 0) {
		var myvalues = [random_1_100(),random_1_100(),random_1_100(),random_1_100(),random_1_100(),random_1_100()];
		$('.dynamicsparkline').sparkline(myvalues);
		window.chart_filter_rendered = 1;
	// };
}

function start_touch_point_chart() {
	if (window.chart_touch_point_rendered == 0) {

		// Get the context of the canvas element we want to select
		var ctx = document.getElementById("donut-2").getContext("2d");
		var myNewChart = new Chart(ctx).Doughnut(top5brand);
		var ctx = document.getElementById("donut-3").getContext("2d");
		var myNewChart = new Chart(ctx).Doughnut(top5brand);
		var ctx = document.getElementById("bar-1").getContext("2d");
		var myBarChart = new Chart(ctx).Bar(barData);
		var ctx = document.getElementById("bar-2").getContext("2d");
		var myBarChart = new Chart(ctx).Bar(barData);

		window.chart_touch_point_rendered = 1;
	};

	var myvalues = [random_1_100(),random_1_100(),random_1_100(),random_1_100(),random_1_100(),random_1_100()];
	$('.dynamicsparkline').sparkline(myvalues,{
		height : 20,
		width : 25
	});
}

function start_touch_point_link_chart() {
	if (window.chart_touch_point_link_rendered == 0) {
		var ctx = document.getElementById("bar-3").getContext("2d");
		var myBarChart = new Chart(ctx).Bar(barData);
		window.chart_touch_point_link_rendered = 1;

		window.chart_touch_point_ksi_rendered = 1;
	};

		var myvalues = [random_1_100(),random_1_100(),random_1_100(),random_1_100(),random_1_100(),random_1_100()];
		$('.dynamicsparkline').sparkline(myvalues,{
		});
}

function start_touch_point_ksi_chart() {
	// if (window.chart_touch_point_ksi_rendered == 0) {
		var myvalues = [random_1_100(),random_1_100(),random_1_100(),random_1_100(),random_1_100(),random_1_100()];
		$('.dynamicsparkline').sparkline(myvalues,{
			height : 20,
			width : 25
		});
		// window.chart_touch_point_ksi_rendered = 1;
	// };
}

function start_top_5_chart() {
	if (window.chart_top_5_rendered == 0) {
		var ctx = document.getElementById("line-top-5").getContext("2d");
		var myBarChart = new Chart(ctx).Line(top5brandLine);
		window.chart_top_5_rendered = 1;
	};
}

function start_filter_map() {
	// if (window.map_filter_rendered == 0) {
		window.map = L.mapbox.map('touch-point-map');
		var stamenLayer = L.tileLayer('img/tile/{x}_{y}_{z}.png', {
		  attribution: 'Map tiles by <a href="http://saklik.com">Saklik </a>.'
		}).addTo(map);

		map.setView([51.5020, -0.1239], 12);

		window.myLayer = L.mapbox.featureLayer().addTo(map);

		myLayer.on('layeradd', function(e) {
			var marker = e.layer,
				feature = marker.feature;
			marker.setIcon(L.icon(feature.properties.icon));
		});

		myLayer.setGeoJSON(generateGeoJson);

		var polygon = L.polygon(line_points, polyline_options).addTo(map);

		myLayer.eachLayer(function(layer) {

			var content = '<h2>' + layer.feature.properties.title + '<\/h2>' +
				'<img width="150" height="auto" src="' + layer.feature.properties.image + '" \/><br \/>' +
				'<p> ' + layer.feature.properties.description + '<\/p>'
				;
			layer.bindPopup(content);
		});

		window.map_filter_rendered = 1;
	// };
}

function start_touch_point_connection_map() {

	if (window.map_cross_channel_rendered == 0) {
		map = L.mapbox.map('touch-point-connection-map');
		stamenLayer = L.tileLayer('img/tile/{x}_{y}_{z}.png', {
			attribution: 'Map tiles by <a href="http://saklik.com">Saklik </a>.'
		}).addTo(map);

		map.setView([51.5020, -0.1239], 12);


		var myLayer = L.mapbox.featureLayer().addTo(map);

		myLayer.on('layeradd', function(e) {
			var marker = e.layer,
				feature = marker.feature;
			marker.setIcon(L.icon(feature.properties.icon));
		});
		
		var polyline = L.polyline([[51.5020, -0.1239], [51.5120, -0.1339]], {color: '#000'}).addTo(map);

		myLayer.setGeoJSON(geoJson);

		myLayer.eachLayer(function(layer) {

			var content = '<h2>' + layer.feature.properties.title + '<\/h2>' +
				'<img width="150" height="auto" src="' + layer.feature.properties.image + '" \/><br \/>' +
				'<p> ' + layer.feature.properties.description + '<\/p>'
				;
			layer.bindPopup(content);
		});

		window.map_cross_channel_rendered = 1;
	};
}

function update_filter_list(){
	$('#current-filter-title').text('Unsaved Filter');
	$('#current-filter-save').removeClass('hide');
	target_container = $('#current-filter-value');
	target_container.html('');
	list_of_filters = _.sortBy(window.current_filter, 'title');
	$.each(list_of_filters, function(index, val) {
		visual = '<li><a href="#" data-dismiss="alert" class="label label-' + val.colorCode +'" title="'+val.title+'" value="'+ val.value +'">' + val.title +' : '+ val.value + ' <span class="glyphicon glyphicon-remove"></span></a></li>';
		target_container.append(visual);
	});		
}

function update_geo_json(){
	var new_filter = [];
	var filtered_geo_json = $.extend(new_filter,generateGeoJson);
	var filter_current = window.current_filter;
	var filter_available = [
		'revenue',
		'amount_of_consumer',
		'brand',
		'stock',
	];
	var valid_aoc = _.findWhere(filter_current,{slug : 'amount_of_consumer'});
	var valid_revenue = _.findWhere(filter_current,{slug : 'revenue'});
	var valid_brand = _.findWhere(filter_current,{slug : 'brand'});
	var valid_stock = _.findWhere(filter_current,{slug : 'stock'});

	console.log('before : ' + generateGeoJson.length);
	

	$.each(filtered_geo_json, function(index, val) {
		if ( valid_aoc !== undefined && val !== undefined &&  -1 === $.inArray(val.amount_of_consumer, valid_aoc.value)) {
			// console.log('filtering : ' + valid_aoc.value);
			filtered_geo_json.splice(index,1);
		};
		if ( valid_revenue !== undefined && val !== undefined &&  -1 === $.inArray(val.revenue, valid_revenue.value)) {
			// console.log('filtering : ' + valid_revenue.value);
			filtered_geo_json.splice(index,1);
		};
		if ( valid_brand !== undefined && val !== undefined &&  val.brand >= valid_brand.value[0] && val.brand <= valid_brand.value[1]) {
			// console.log('filtering : ' + valid_brand.value);
			filtered_geo_json.splice(index,1);
		};
		if ( valid_stock !== undefined && val !== undefined &&  val.stock >= valid_stock.value[0] && val.stock <= valid_stock.value[1]) {
			// console.log('filtering : ' + valid_stock.value);
			filtered_geo_json.splice(index,1);
		};
	});

	console.log('after : ' + filtered_geo_json.length);

	window.filtered_geo_json = filtered_geo_json;
	window.myLayer.setGeoJSON(filtered_geo_json);

	window.myLayer.eachLayer(function(layer) {

		var content = '<h2>' + layer.feature.properties.title + '<\/h2>' +
			'<img width="150" height="auto" src="' + layer.feature.properties.image + '" \/><br \/>' +
			'<p> ' + layer.feature.properties.description + '<\/p>'
			;
		layer.bindPopup(content);
	});

	// start_filter_map();
	
	


	// filtered_geo_json = _.filter(generateGeoJson, function(marker){
	// 	return num % 2 == 0;
	// });

}

$(document).ready(function() {

	//chart status
	window.chart_dashboard_rendered = 0;
	window.chart_filter_rendered = 0; // sparkline
	window.chart_touch_point_rendered = 0;
	window.chart_touch_point_link_rendered = 0;
	window.chart_touch_point_ksi_rendered = 0; // sparkline
	window.chart_top_5_rendered = 0;

	// map status
	window.map_filter_rendered = 0;	
	window.map_cross_channel_rendered = 0;

	// filter definition
	window.current_filter = [];

	//dev
	sign_in();
	show_touchPoints();

	// navigation : login
	$("#form-signin").submit(function( event ) {
		event.preventDefault();
		sign_in();
		show_dashboard();
	});

	// navigation : logged in
	$('#main-navigation a').click(function(event) {
		target = $(this).attr('href')
		$('#main-navigation').offcanvas('hide');
		switch(target) {
			case "#dashboard":
				show_dashboard();
				break;
			case "#touch-points":
				show_touchPoints();
				break;
			case "#notifications":
				alert("#notifications");
				break;
			case "#signout":
				sign_out();
				break;
			case "#management":
				show_management();
				break;
			default:
				alert("huh");
		}
	});

	// navigation : dashboard
	$('.dashboard-link').click(function(event) {
		target = $(this).attr('href')
		switch(target) {
			case "#touch-points":
				show_touchPoints();
				break;
			case "#management":
				show_management();
				break;
			default:
				alert("huh");
		}
	});

	// navigation : dashboard
	$('#result-display table a').click(function(event) {
		target = $(this).attr('href')
		switch(target) {
			case "#single-touch-point":
				show_single_touchPoints();
				break;
			default:
				alert("huh");
		}
	});

	//action : show line chart modal
	$('#donut-0, #donut-1, #donut-2, #donut-3').click(function(event) {
		$('#top-5-brand').modal('show');
		start_top_5_chart();
	});

	// action : generate connection map
	$('a[href="#touch-point-cross-channel"]').on('shown.bs.tab', function (e) {
		start_touch_point_connection_map();
		start_touch_point_link_chart();
	});

	// action : generate touch points sparkline
	$('a[href="#result-as-table"]').on('shown.bs.tab', function (e) {
		start_filter_chart();
	});

	// action : generate touch points sparkline
	$('a[href="#touch-point-ksi"]').on('shown.bs.tab', function (e) {
		start_touch_point_ksi_chart();
	});

	// action : add tag filter definition
	$('#filter-option a').click(function(event) {
		event.preventDefault();
		var preNewFilter = {};
		preNewFilter.title = $(this).text();
		preNewFilter.colorCode = $(this).data('color');
		preNewFilter.slug = $(this).data('filter');
		target = $(this).attr('href');
		$(target).find('.modal-header').removeClass('btn-primary btn-success btn-info btn-warning').addClass('btn-'+preNewFilter.colorCode);
		$(target).find('.filter-name').text($(this).text());
		if (target == '#new-slider-filter') {
			preNewFilter.type = 'slider';
			preNewFilter.value = [0,100];
		} else {
			preNewFilter.type = 'tag';
		};
		window.preNewFilter = preNewFilter;
	});

	// action : submit tag filter
	$('.tag-filter .filter-submit').click(function(event) {
		event.preventDefault();
		// var added_filter = [];
		var newFilter = {};
		var added_value = [];
		console.log(preNewFilter);
		$('.define-filter .filter-values .active').each(function(index, el) {
			// var newFilter = {};
			// newFilter.value = $(this).attr('value');
			// $.extend( newFilter, window.preNewFilter );
			// window.current_filter.push(newFilter);
			added_value.push($(this).attr('value'));

		});
		$.extend( newFilter, window.preNewFilter );
		newFilter.value = added_value;
		window.current_filter.push(newFilter);
		$('.define-filter .filter-values .active').removeClass('active');
		update_filter_list();
		update_geo_json();
	});

	// action : remove a filter from filter list
	$('#current-filter-value').on('click', 'a', function(event) {
		event.preventDefault();
		title = $(this).attr('title');
		value = $(this).attr('value');
		$.each(window.current_filter, function(index, val) {
			if (val.title == title && val.value == value) {
				if (index > -1) {
					window.current_filter.splice(index, 1);
				};
			};
		});
		if (window.current_filter.length == 0) {
			$('#current-filter-title').text('No Filters');
			$('#current-filter-save').addClass('hide');
		}
		update_geo_json();
	});

	// action : go to touch point detail
	$('#touch-point-map').on('click', '.leaflet-popup-content', function() {
		event.preventDefault();
		show_single_touchPoints();
	});

	// action : add slide filter definition
	$("#slider-input").on({
		set : function(){
			window.preNewFilter.value = $(this).val();
		}
	});

	// action : submit slider filter
	$('.slider-filter .filter-submit').click(function(event) {
		if('value' in window.preNewFilter){
		} else {
			window.preNewFilter.value = [0,100];
		}
		window.current_filter.push(window.preNewFilter);
		update_filter_list();
		update_geo_json();
	});

	// action : load filter
	$('#load-filter-1').click(function(event) {
		event.preventDefault();
		$('#saved-filters').modal('hide');
		window.current_filter = saved_filter.filters;
		update_filter_list();
		$('#current-filter-title').text(saved_filter.title);
	});

	// 
	// ui methods
	//


		// component : btn-group as checkboxes
		$('.filter-values .btn').each(function () {
			$(this).on('click', function(event) {
				$(this).toggleClass('active');
				event.preventDefault();
				/* Act on the event */
			});
		});

		// component : ui slider
		$("#slider-input").noUiSlider({
			start: [0, 100],
			step:10,
			connect: true,
			range: {
				'min': 0,
				'max': 100
			},
			serialization: {
				lower: [
					$.Link({
						target: $("#value-input")
					})
				],
				upper: [
					$.Link({
						target: $("#value-span"),
					})
				],
				format: {
					decimals: 0
				}
			}
		});

		// component : button as radio button
		$('.fi-radio a').on('click', function(){
			$(this).removeClass('notActive').addClass('active');
			$(this).siblings('a').removeClass('active').addClass('notActive');	
		})
});