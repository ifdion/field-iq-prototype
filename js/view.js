/*global Canvasing, Backbone, JST*/

Canvasing.Views = Canvasing.Views || {};

(function () {
	'use strict';

	Canvasing.Views.Login = Backbone.View.extend({

		tagName: 'div',

		id: 'login-screen',

		className: '',

		events: {
			'submit #form-signin' : 'signIn',
		},

		initialize: function () {
			// this.listenTo(this.model, 'change', this.render);
		},

		render: function () {
			this.$el.html(_.template($('#login-screen').html()));
			// this.$el.html(this.template());
			return this;
		},

		signIn : function(ev) {

			// browser testing
			console.log('signing in the user');
			$('#login-screen').fadeOut('fast').;
			Canvasing.Routers.router.navigate('dashboard',{trigger: true});
			return false;
		},

	});

})();