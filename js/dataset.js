function random_1_100(){
	// return random number
	return Math.floor((Math.random() * 100) + 1);
}

function random_1_10(){
	// return random number
	return Math.floor((Math.random() * 10) + 1);
}

function random_1_x(n){
	// return random number
	return Math.floor((Math.random() * n) + 1);
}

var top5brand = [
	{
		value: 20,
		color:"#FF5A5E",
		label: "XLA"
	},
	{
		value: 10,
		color:"#5AD3D1",
		label: "AVX"
	},
	{
		value: 30,
		color:"#FFC870",
		label: "DLC"
	},
	{
		value: 40,
		color: "#0AA3D1",
		label: "Memp"
	},
	{
		value: 50,
		color: "#99AA99",
		label: "MAC"
	}
];

var top5brandLine = {
	labels: ["Mon(05/7)", "Tue(06/7)", "Wed(08/7)", "Thu(09/7)", "Fri(10/7)", "Sat(11/7)", "Sun(12/7)"],
	datasets: [
		{
			label: "XLA",
			fillColor: "transparent",
			strokeColor: "#FF5A5E",
			pointColor: "#FF5A5E",
			data: [10, 40, 10, 40, 20, 30, 20]
		},
		{
			label: "AVX",
			fillColor: "transparent",
			strokeColor: "#5AD3D1",
			pointColor: "#5AD3D1",
			data: [10, 30, 20, 10, 30, 10, 10]
		},
		{
			label: "DLC",
			fillColor: "transparent",
			strokeColor: "#FFC870",
			pointColor: "#FFC870",
			data: [40, 50, 30, 20, 10, 20, 30]
		},
		{
			label: "Memp",
			fillColor: "transparent",
			strokeColor: "#0AA3D1",
			pointColor: "#0AA3D1",
			data: [30, 20, 40, 50, 50, 40, 40]
		},
		{
			label: "MAC",
			fillColor: "transparent",
			strokeColor: "#99AA99",
			pointColor: "#99AA99",
			data: [20, 10, 20, 30, 40, 50, 50]
		}
	]
};

var salesBar = {
	labels: ["Mon(05/7)", "Tue(06/7)", "Wed(08/7)", "Thu(09/7)", "Fri(10/7)", "Sat(11/7)", "Sun(12/7)"],
	datasets: [
		{
			label: "Sales",
			fillColor: "#777",
			data: [25, 40, 40, 61, 56, 55, 40]
		},
		{
			label: "Stock",
			fillColor: "#428bca",
			data: [28, 48, 40, 80, 86, 86, 90]
		}
	]
};

var ksiOverviewBar = {
	labels: ["Mon(05/7)", "Tue(06/7)", "Wed(08/7)", "Thu(09/7)", "Fri(10/7)", "Sat(11/7)", "Sun(12/7)"],
	datasets: [
		{
			label: "Stock",
			fillColor: "#777",
			data: [28, 48, 40, 80, 86, 86, 90]
		},{
			label: "Handling",
			fillColor: "#428bca",
			data: [25, 40, 43, 61, 56, 55, 40]
		},{
			label: "OoS",
			fillColor: "#5bc0de",
			data: [28, 53, 49, 55, 83, 88, 80]
		},{
			label: "OoSC",
			fillColor: "#f0ad4e",
			data: [28, 44, 49, 72, 80, 80, 76]
		},{
			label: "Visibility",
			fillColor: "#5cb85c",
			data: [28, 35, 41, 64, 70, 76, 72]
		}
	]
};


//Generates some data. This step is of course normally done by your web server.
function getData() {

    //First define the columns
    var cols = {
        productName: {
            index: 1, //The order this column should appear in the table
            type: "number", //The type. Possible are string, number, bool, date(in milliseconds).
            friendly: "Product Name",  //Name that will be used in header. Can also be any html as shown here.
            // unique: true,  //This is required if you want checkable rows, or to use the rowClicked callback. Be certain the values are really unique or weird things will happen.
            sortOrder: "asc", //Data will initially be sorted by this column. Possible are "asc" or "desc"
        },
        handling: {
            index: 2,
            type: "number",
            format: "<a href='#' class='userId' target='_blank'>{0}</a>",  //Used to format the data anything you want. Use {0} as placeholder for the actual data.
            friendly: "Handling",
        },
        oos: {
            index: 3,
            type: "number",
            format: "<a href='#' class='userId' target='_blank'>{0}</a>",  //Used to format the data anything you want. Use {0} as placeholder for the actual data.
            friendly: "OoS",
        },
        oosc: {
            index: 4,
            type: "number",
            format: "<a href='#' class='userId' target='_blank'>{0}</a>",  //Used to format the data anything you want. Use {0} as placeholder for the actual data.
            decimals: 2, //Force decimal precision
            friendly: "OoSC",
        },
        visibility: {
            index: 6,
            type: "number",
            format: "<a href='#' class='userId' target='_blank'>{0}</a>",  //Used to format the data anything you want. Use {0} as placeholder for the actual data.
            friendly: "Visibility"
        },
        effCall: {
            index: 7,
            type: "number", //Don't forget dates are expressed in milliseconds
            format: "<a href='#' class='userId' target='_blank'>{0}</a>",  //Used to format the data anything you want. Use {0} as placeholder for the actual data.
            friendly: "Eff. Call"
        },
        avPurchase: {
            index: 8,
            type: "number", //Don't forget dates are expressed in milliseconds
            format: "<a href='#' class='userId' target='_blank'>{0}</a>",  //Used to format the data anything you want. Use {0} as placeholder for the actual data.
            friendly: "Av. Purchase"
        }
    };

    /*
      Create the actual data.
      Whats worth mentioning is that you can use a 'format' property just as in the column definition,
      but on a row level. See below on how we create a weightFormat property. This will be used when rendering the weight column.
      Also, you can pre-check rows with the 'checked' property and prevent rows from being checkable with the 'checkable' property.
    */
    var rows = [];
    var i = 1;
    while(i <= 1000)
    {
        var weight = (Math.floor(Math.random()*40)+50) + (Math.floor(Math.random()*100)/100);
        var weightClass = weight <70 ? 'green' : weight <80 && weight >=70 ? 'yellow' : 'red';

        //We leave some fields intentionally undefined, so you can see how sorting/filtering works with these.
        var doc = {
            userId: i,
            name: i%100 == 0 ? undefined : elfName(),
            age: Math.floor(Math.random()*50)+20,
            weight: weight > 50 && weight < 60 ? undefined:weight,
            weightFormat:  "<div class='" + weightClass + "'>{0}</div>",
            height: Math.floor(Math.random()*50)+150,
            important: i%3 == 0 ? undefined : i%4 == 0,
            someDate: i%4 == 0
                ? undefined
                : Date.now() + (i*Math.floor(Math.random()*(60*60*24*100))),
            checkable: i % 4 != 0,
            checked: i % 3 == 0
        };
        rows.push(doc);
        i++;
    }

    //Create the returning object. Besides cols and rows, you can also pass any other object you would need later on.
    var data = {
        cols: cols,
        rows: rows,
        otherStuff: {
            thatIMight: 1,
            needLater: true
        }
    };

    return data;
}

//Helper function to generate names
function elfName() {
    var elf_male = new Array("Abardon", "Acaman", "Achard", "Ackmard", "Agon", "Agnar", "Abdun", "Aidan", "Airis", "Aldaren", "Alderman", "Alkirk", "Amerdan", "Anfarc", "Aslan", "Actar", "Atgur", "Atlin", "Aldan", "Badek", "Baduk", "Bedic", "Beeron", "Bein", "Bithon", "Bohl", "Boldel", "Bolrock", "Bredin", "Bredock", "Breen", "tristan", "Bydern", "Cainon", "Calden", "Camon", "Cardon", "Casdon", "Celthric", "Cevelt", "Chamon", "Chidak", "Cibrock", "Cipyar", "Colthan", "Connell", "Cordale", "Cos", "Cyton", "Daburn", "Dawood", "Dak", "Dakamon", "Darkboon", "Dark", "Darg", "Darmor", "Darpick", "Dask", "Deathmar", "Derik", "Dismer", "Dokohan", "Doran", "Dorn", "Dosman", "Draghone", "Drit", "Driz", "Drophar", "Durmark", "Dusaro", "Eckard", "Efar", "Egmardern", "Elvar", "Elmut", "Eli", "Elik", "Elson", "Elthin", "Elbane", "Eldor", "Elidin", "Eloon", "Enro", "Erik", "Erim", "Eritai", "Escariet", "Espardo", "Etar", "Eldar", "Elthen", "Elfdorn", "Etran", "Eythil", "Fearlock", "Fenrirr", "Fildon", "Firdorn", "Florian", "Folmer", "Fronar", "Fydar", "Gai", "Galin", "Galiron", "Gametris", "Gauthus", "Gehardt", "Gemedes", "Gefirr", "Gibolt", "Geth", "Gom", "Gosform", "Gothar", "Gothor", "Greste", "Grim", "Gryni", "Gundir", "Gustov", "Halmar", "Haston", "Hectar", "Hecton", "Helmon", "Hermedes", "Hezaq", "Hildar", "Idon", "Ieli", "Ipdorn", "Ibfist", "Iroldak", "Ixen", "Ixil", "Izic", "Jamik", "Jethol", "Jihb", "Jibar", "Jhin", "Julthor", "Justahl", "Kafar", "Kaldar", "Kelar", "Keran", "Kib", "Kilden", "Kilbas", "Kildar", "Kimdar", "Kilder", "Koldof", "Kylrad", "Lackus", "Lacspor", "Lahorn", "Laracal", "Ledal", "Leith", "Lalfar", "Lerin", "Letor", "Lidorn", "Lich", "Loban", "Lox", "Ludok", "Ladok", "Lupin", "Lurd", "Mardin", "Markard", "Merklin", "Mathar", "Meldin", "Merdon", "Meridan", "Mezo", "Migorn", "Milen", "Mitar", "Modric", "Modum", "Madon", "Mafur", "Mujardin", "Mylo", "Mythik", "Nalfar", "Nadorn", "Naphazw", "Neowald", "Nildale", "Nizel", "Nilex", "Niktohal", "Niro", "Nothar", "Nathon", "Nadale", "Nythil", "Ozhar", "Oceloth", "Odeir", "Ohmar", "Orin", "Oxpar", "Othelen", "Padan", "Palid", "Palpur", "Peitar", "Pendus", "Penduhl", "Pildoor", "Puthor", "Phar", "Phalloz", "Qidan", "Quid", "Qupar", "Randar", "Raydan", "Reaper", "Relboron", "Riandur", "Rikar", "Rismak", "Riss", "Ritic", "Ryodan", "Rysdan", "Rythen", "Rythorn", "Sabalz", "Sadaron", "Safize", "Samon", "Samot", "Secor", "Sedar", "Senic", "Santhil", "Sermak", "Seryth", "Seth", "Shane", "Shard", "Shardo", "Shillen", "Silco", "Sildo", "Silpal", "Sithik", "Soderman", "Sothale", "Staph", "Suktar", "zuth", "Sutlin", "Syr", "Syth", "Sythril", "Talberon", "Telpur", "Temil", "Tamilfist", "Tempist", "Teslanar", "Tespan", "Tesio", "Thiltran", "Tholan", "Tibers", "Tibolt", "Thol", "Tildor", "Tilthan", "Tobaz", "Todal", "Tothale", "Touck", "Tok", "Tuscan", "Tusdar", "Tyden", "Uerthe", "Uhmar", "Uhrd", "Updar", "Uther", "Vacon", "Valker", "Valyn", "Vectomon", "Veldar", "Velpar", "Vethelot", "Vildher", "Vigoth", "Vilan", "Vildar", "Vi", "Vinkol", "Virdo", "Voltain", "Wanar", "Wekmar", "Weshin", "Witfar", "Wrathran", "Waytel", "Wathmon", "Wider", "Wyeth", "Xandar", "Xavor", "Xenil", "Xelx", "Xithyl", "Yerpal", "Yesirn", "Ylzik", "Zak", "Zek", "Zerin", "Zestor", "Zidar", "Zigmal", "Zilex", "Zilz", "Zio", "Zotar", "Zutar", "Zytan");
    var elf_female = new Array("Acele Acholate", "Ada", "Adiannon", "Adorra", "Ahanna", "Akara", "Akassa", "Akia", "Amara", "Amarisa", "Amarizi", "Ana", "Andonna", "Ariannona", "Arina", "Arryn", "Asada", "Awnia", "Ayne", "Basete", "Bathelie", "Bethel", "Brana", "Brynhilde", "Calene", "Calina", "Celestine", "Corda", "Enaldie", "Enoka", "Enoona", "Errinaya", "Fayne", "Frederika", "Frida", "Gvene", "Gwethana", "Helenia", "Hildandi", "Helvetica", "Idona", "Irina", "Irene", "Illia", "Irona", "Justalyne", "Kassina", "Kilia", "Kressara", "Laela", "Laenaya", "Lelani", "Luna", "Linyah", "Lyna", "Lynessa", "Mehande", "Melisande", "Midiga", "Mirayam", "Mylene", "Naria", "Narisa", "Nelena", "Nimaya", "Nymia", "Ochala", "Olivia", "Onathe", "Parthinia", "Philadona", "Prisane", "Rhyna", "Rivatha", "Ryiah", "Sanata", "Sathe", "Senira", "Sennetta", "Serane", "Sevestra", "Sidara", "Sidathe", "Sina", "Sunete", "Synestra", "Sythini", "zena", "Tabithi", "Tomara", "Teressa", "Tonica", "Thea", "Teressa", "Urda", "Usara", "Useli", "Unessa", "ursula", "Venessa", "Wanera", "Wellisa", "yeta", "Ysane", "Yve", "Yviene", "Zana", "Zathe", "Zecele", "Zenobe", "Zema", "Zestia", "Zilka", "Zoucka", "Zona", "Zyneste", "Zynoa");
    var elf_surname = new Array("Abardon", "Acaman", "Achard", "Ackmard", "Agon", "Agnar", "Aldan", "Abdun", "Aidan", "Airis", "Aldaren", "Alderman", "Alkirk", "Amerdan", "Anfarc", "Aslan", "Actar", "Atgur", "Atlin", "Badek", "Baduk", "Bedic", "Beeron", "Bein", "Bithon", "Bohl", "Boldel", "Bolrock", "Bredin", "Bredock", "Breen", "tristan", "Bydern", "Cainon", "Calden", "Camon", "Cardon", "Casdon", "Celthric", "Cevelt", "Chamon", "Chidak", "Cibrock", "Cipyar", "Colthan", "Connell", "Cordale", "Cos", "Cyton", "Daburn", "Dawood", "Dak", "Dakamon", "Darkboon", "Dark", "Dark", "Darmor", "Darpick", "Dask", "Deathmar", "Derik", "Dismer", "Dokohan", "Doran", "Dorn", "Dosman", "Draghone", "Drit", "Driz", "Drophar", "Durmark", "Dusaro", "Eckard", "Efar", "Egmardern", "Elvar", "Elmut", "Eli", "Elik", "Elson", "Elthin", "Elbane", "Eldor", "Elidin", "Eloon", "Enro", "Erik", "Erim", "Eritai", "Escariet", "Espardo", "Etar", "Eldar", "Elthen", "Etran", "Eythil", "Fearlock", "Fenrirr", "Fildon", "Firdorn", "Florian", "Folmer", "Fronar", "Fydar", "Gai", "Galin", "Galiron", "Gametris", "Gauthus", "Gehardt", "Gemedes", "Gefirr", "Gibolt", "Geth", "Gom", "Gosform", "Gothar", "Gothor", "Greste", "Grim", "Gryni", "Gundir", "Gustov", "Halmar", "Haston", "Hectar", "Hecton", "Helmon", "Hermedes", "Hezaq", "Hildar", "Idon", "Ieli", "Ipdorn", "Ibfist", "Iroldak", "Ixen", "Ixil", "Izic", "Jamik", "Jethol", "Jihb", "Jibar", "Jhin", "Julthor", "Justahl", "Kafar", "Kaldar", "Kelar", "Keran", "Kib", "Kilden", "Kilbas", "Kildar", "Kimdar", "Kilder", "Koldof", "Kylrad", "Lackus", "Lacspor", "Lahorn", "Laracal", "Ledal", "Leith", "Lalfar", "Lerin", "Letor", "Lidorn", "Lich", "Loban", "Lox", "Ludok", "Ladok", "Lupin", "Lurd", "Mardin", "Markard", "Merklin", "Mathar", "Meldin", "Merdon", "Meridan", "Mezo", "Migorn", "Milen", "Mitar", "Modric", "Modum", "Madon", "Mafur", "Mujardin", "Mylo", "Mythik", "Nalfar", "Nadorn", "Naphazw", "Neowald", "Nildale", "Nizel", "Nilex", "Niktohal", "Niro", "Nothar", "Nathon", "Nadale", "Nythil", "Ozhar", "Oceloth", "Odeir", "Ohmar", "Orin", "Oxpar", "Othelen", "Padan", "Palid", "Palpur", "Peitar", "Pendus", "Penduhl", "Pildoor", "Puthor", "Phar", "Phalloz", "Qidan", "Quid", "Qupar", "Randar", "Raydan", "Reaper", "Relboron", "Riandur", "Rikar", "Rismak", "Riss", "Ritic", "Ryodan", "Rysdan", "Rythen", "Rythorn", "Sabalz", "Sadaron", "Safize", "Samon", "Samot", "Secor", "Sedar", "Senic", "Santhil", "Sermak", "Seryth", "Seth", "Shane", "Shard", "Shardo", "Shillen", "Silco", "Sildo", "Silpal", "Sithik", "Soderman", "Sothale", "Staph", "Suktar", "zuth", "Sutlin", "Syr", "Syth", "Sythril", "Talberon", "Telpur", "Temil", "Tamilfist", "Tempist", "Teslanar", "Tespan", "Tesio", "Thiltran", "Tholan", "Tibers", "Tibolt", "Thol", "Tildor", "Tilthan", "Tobaz", "Todal", "Tothale", "Touck", "Tok", "Tuscan", "Tusdar", "Tyden", "Uerthe", "Uhmar", "Uhrd", "Updar", "Uther", "Vacon", "Valker", "Valyn", "Vectomon", "Veldar", "Velpar", "Vethelot", "Vildher", "Vigoth", "Vilan", "Vildar", "Vi", "Vinkol", "Virdo", "Voltain", "Wanar", "Wekmar", "Weshin", "Witfar", "Wrathran", "Waytel", "Wathmon", "Wider", "Wyeth", "Xandar", "Xavor", "Xenil", "Xelx", "Xithyl", "Yerpal", "Yesirn", "Ylzik", "Zak", "Zek", "Zerin", "Zestor", "Zidar", "Zigmal", "Zilex", "Zilz", "Zio", "Zotar", "Zutar", "Zytan");
    var prefix_male = elf_male;
    var prefix_female = elf_female;
    var suffix = elf_surname;
    var n1m = parseInt(Math.random() * prefix_male.length);
    var n1f = parseInt(Math.random() * prefix_female.length);
    var n2 = parseInt(Math.random() * suffix.length);
    var n2ekstra = parseInt(Math.random() * suffix.length);
    var extraname = "extranahme";
    var prename_male = prefix_male[n1m].slice(0, 1).toUpperCase() + prefix_male[n1m].slice(1);
    var prename_female = prefix_female[n1f].slice(0, 1).toUpperCase() + prefix_female[n1f].slice(1);
    var sufname = suffix[n2].slice(0, 1).toUpperCase() + suffix[n2].slice(1);
    var extraname = suffix[n2ekstra].slice(0, 1).toUpperCase() + suffix[n2ekstra].slice(1);
    var n3 = parseInt(Math.random() * 100);
    if (n3 <= 40) {
        name = prename_male + " " + sufname
    } else if (n3 > 40 && n3 <= 70) {
        name = prename_female + " " + sufname
    }
    return name;
}


var front_name = [ 'Ajat', 'Rohmat','Ceni', 'Mamat', 'Bari','Samiun','Jaja','Sampeni','Koko','Subeni'];
var last_name = [ 'Bahagia', 'Jujur','Sejahtera', 'Ceria', 'Santun','Bebas','Cinta','Tulus','Semangat','Baru'];

var barData = {
	labels: ["January", "February", "March", "April", "May", "June", "July"],
	datasets: [
		{
			label: "My First dataset",
			fillColor: "rgba(220,220,220,0.5)",
			strokeColor: "rgba(220,220,220,0.8)",
			highlightFill: "rgba(220,220,220,0.75)",
			highlightStroke: "rgba(220,220,220,1)",
			data: [65, 59, 80, 81, 56, 55, 40]
		},
		{
			label: "My Second dataset",
			fillColor: "rgba(151,187,205,0.5)",
			strokeColor: "rgba(151,187,205,0.8)",
			highlightFill: "rgba(151,187,205,0.75)",
			highlightStroke: "rgba(151,187,205,1)",
			data: [28, 48, 40, 19, 86, 27, 90]
		}
	]
};

var geoJson = [
	{
		"type": "Feature",
		'Revenue' : 'value-1',
		"geometry": {
			"type": "Point",
			"coordinates": [-0.1239, 51.5020]
		},
		"properties": {
			"title": "Toko Ajat",
			"description" : '21 Westminister',
			'image' : 'img/profile-1.png',
			"icon": {
				"iconUrl": "img/marker/black-cafe.png",
				"iconSize": [35, 90],
				"iconAnchor": [17.5, 45],
				"popupAnchor": [0, -45],
				"className": "dot"
			}
		}
	},{
		"type": "Feature",
		'Revenue' : 'value-2',
		"geometry": {
			"type": "Point",
			"coordinates": [-0.1339, 51.5120]
		},
		"properties": {
			"title": "Warung Coco",
			"description" : '22 Westminister',
			'image' : 'img/profile-2.png',
			"icon": {
				"iconUrl": "img/marker/gray-cafe.png",
				"iconSize": [35, 90],
				"iconAnchor": [17.5, 45],
				"popupAnchor": [0, -45],
				"className": "dot"
			}
		}
	},{
		"type": "Feature",
		'Revenue' : 'value-2',
		"geometry": {
			"type": "Point",
			"coordinates": [-0.1239, 51.4920]
		},
		"properties": {
			"title": "Warung Ceny",
			"description" : '22 Westminister',
			'image' : 'img/profile-2.png',
			"icon": {
				"iconUrl": "img/marker/gray-cafe.png",
				"iconSize": [35, 90],
				"iconAnchor": [17.5, 45],
				"popupAnchor": [0, -45],
				"className": "dot"
			}
		}
	}
];

var generateGeoJson = [];

for (var i = 5145.90; i <= 5155.90; i += 3) {
	for (var j = -21.5; j <= -1.5; j += 3) {

		revenue = random_1_10();
		brand = random_1_100();
		color = random_1_x(3);

		var marker =  {
			"type": "Feature",
			'revenue' : 'value-' + revenue ,
			'amount_of_consumer' : 'value-' + revenue ,
			'brand' : brand,
			'stock' : brand,
			"geometry": {
				"type": "Point",
				"coordinates": []
			},
			"properties": {
				"title": "Toko " + front_name[random_1_10() - 1] + ' '+ last_name[random_1_10() - 1],
				"description" :  ' Jalan ' +last_name[random_1_10() - 1] + ' No ' +random_1_100() + ' London <br> <a href=#>See Detail</a>',
				'image' : 'img/profile-1.png',
				"icon": {
					"iconUrl": "img/marker/pin-"+color+".png",
					"iconSize": [35, 90],
					"iconAnchor": [17.5, 45],
					"popupAnchor": [0, -45],
					"className": "dot"
				}
			}
		}
		marker.geometry.coordinates.push(j / 100);
		marker.geometry.coordinates.push(i / 100);
		generateGeoJson.push(marker);
	}
};

var line_points = [
    [51.4520,-0.2239],
    [51.5520,-0.2239],
    [51.5520,-0.0239],
    [51.4520,-0.0239],
];

var polyline_options = {
    color: '#000',
    stroke: false
};

var saved_filter = {
	'title' : 'Custom Filter #1',
	'filters' : [
		{
			'colorCode': 'primary',
			'title': 'Revenue',
			'value': 'value-1'
		},
		{
			'colorCode': 'primary',
			'title': 'Revenue',
			'value': 'value-2'
		},
		{
			'colorCode': 'success',
			'title': 'Stock',
			'value': [0,60]
		},
		{
			'colorCode': 'info',
			'title': 'Share of Market',
			'value': 'value-3'
		},
		{
			'colorCode': 'warning',
			'title': 'Operational Hour',
			'value': [50,80]
		}
	]
}